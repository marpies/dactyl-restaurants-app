//
//  ServicesAssembly.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright (c) 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import Swinject

struct ServicesAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(LocationManager.self) { r in
            return ReactiveLocationManager()
        }.inObjectScope(.container)
        
        container.register(APIService.self) { r in
            return DactylAPIService()
        }.inObjectScope(.container)
    }
    
}

//
//  ReactiveLocationManager.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright © 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import CoreLocation
import Combine

class ReactiveLocationManager: NSObject, LocationManager, CLLocationManagerDelegate {
    
    private let locationManager: CLLocationManager = CLLocationManager()
    
    private let locations: PassthroughSubject<[CLLocation], Never> = PassthroughSubject()
    
    private let _authorizationStatus: CurrentValueSubject<CLAuthorizationStatus, Never>
    let authorizationStatus: AnyPublisher<CLAuthorizationStatus, Never>
    
    var currentLocation: AnyPublisher<CLLocation, Never> {
        let publisher = self.locations
            .compactMap(\.first)
            .prefix(1)
            .handleEvents(receiveCompletion: { [weak self] _ in
                self?.locationManager.stopUpdatingLocation()
            })
            .eraseToAnyPublisher()
        
        self.locationManager.startUpdatingLocation()
        
        return publisher
    }
    
    override init() {
        _authorizationStatus = CurrentValueSubject(self.locationManager.currentAuthorizationStatus)
        self.authorizationStatus = _authorizationStatus.eraseToAnyPublisher()
        
        super.init()
        
        self.locationManager.delegate = self
    }
    
    func requestLocation() {
        self.locationManager.requestWhenInUseAuthorization()
    }
    
    //
    // MARK: - Location manager delegate
    //
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        _authorizationStatus.send(status)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locations.send(locations)
    }
    
}


fileprivate extension CLLocationManager {
    
    var currentAuthorizationStatus: CLAuthorizationStatus {
        if #available(iOS 14.0, *) {
            return self.authorizationStatus
        }
        return CLLocationManager.authorizationStatus()
    }
    
}

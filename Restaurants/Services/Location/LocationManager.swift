//
//  LocationManager.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright © 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import CoreLocation
import Combine

protocol LocationManager {
    var currentLocation: AnyPublisher<CLLocation, Never> { get }
    var authorizationStatus: AnyPublisher<CLAuthorizationStatus, Never> { get }
    
    func requestLocation()
}

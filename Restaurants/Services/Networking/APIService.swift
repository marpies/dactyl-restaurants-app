//
//  APIService.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright © 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import Combine

protocol APIService {
    func loadRestaurants() -> AnyPublisher<[Restaurant.Response], Error>
}

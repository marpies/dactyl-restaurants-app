//
//  DactylAPIService.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright © 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import Combine

struct DactylAPIService: APIService {
    
    private let url: URL = URL(string: "https://cms.dgrp.cz/ios/data.json")!
    
    func loadRestaurants() -> AnyPublisher<[Restaurant.Response], Error> {
        return URLSession.shared.dataTaskPublisher(for: self.url)
            .tryMap { (data, response) -> Data in
                guard let httpResponse = response as? HTTPURLResponse,
                      200...299 ~= httpResponse.statusCode else {
                          throw URLError(.badServerResponse)
                      }
                return data
            }
            .decode(type: Restaurants.Response.self, decoder: JSONDecoder())
            .map { $0.restaurants }
            .eraseToAnyPublisher()
    }
    
}

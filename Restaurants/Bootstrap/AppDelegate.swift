//
//  AppDelegate.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright © 2022 Marcel Piešťanský. All rights reserved.
//

import UIKit
import Swinject

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    private let assembler = Assembler()
    private var coordinator: SetupCoordinator?
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Root coordinator
        self.coordinator = SetupCoordinator(assembler: self.assembler)
        
        // Setup window
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = .systemBackground
        self.window?.rootViewController = self.coordinator?.start()
        self.window?.makeKeyAndVisible()
        
        return true
    }
    
}


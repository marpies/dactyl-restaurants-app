//
//  MapViewDelegate.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright © 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import MapKit

class MapViewDelegate: NSObject, MKMapViewDelegate {
    
    private let annotationId = "restaurant"
    private let mapView: MKMapView
    
    init(mapView: MKMapView) {
        self.mapView = mapView
        
        super.init()
        
        self.mapView.delegate = self
        self.mapView.register(RestaurantAnnotationView.self, forAnnotationViewWithReuseIdentifier: self.annotationId)
    }
    
    func addAnnotations(_ annotations: [MKAnnotation]) {
        self.mapView.addAnnotations(annotations)
    }
    
    //
    // MARK: - Map delegate
    //
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is RestaurantAnnotation else { return nil }
        
        let view: RestaurantAnnotationView
        
        if let dequeued = mapView.dequeueReusableAnnotationView(withIdentifier: self.annotationId) as? RestaurantAnnotationView {
            view = dequeued
            view.annotation = annotation
        } else {
            view = RestaurantAnnotationView(annotation: annotation, reuseIdentifier: self.annotationId)
        }
        
        return view
    }
    
}

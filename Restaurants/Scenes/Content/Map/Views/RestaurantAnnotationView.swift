//
//  RestaurantAnnotationView.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright © 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import MapKit

class RestaurantAnnotationView: MKMarkerAnnotationView {
    
    override var annotation: MKAnnotation? {
        didSet {
            guard let annotation = self.annotation else {
                return
            }
            
            self.canShowCallout = true
            
            // Custom label with multiline support
            let label = UILabel()
            label.numberOfLines = 0
            label.font = UIFont.preferredFont(forTextStyle: .caption1)
            label.textColor = .secondaryLabel
            label.text = annotation.subtitle ?? nil
            self.detailCalloutAccessoryView = label
        }
    }
    
}

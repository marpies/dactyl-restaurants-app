//
//  MapViewController.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright (c) 2022 Marcel Piešťanský. All rights reserved.
//

import UIKit
import Combine
import MapKit

class MapViewController: UIViewController {
    
    private let viewModel: MapViewModelProtocol
    private let mapView = MKMapView()
    private lazy var mapDelegate = MapViewDelegate(mapView: self.mapView)
    
    private var cancellables: Set<AnyCancellable> = []

    init(viewModel: MapViewModelProtocol) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .systemBackground
        
        self.setupViews()
        self.bindViewModel()
    }
    
    //
    // MARK: - Private
    //
    
    private func setupViews() {
        self.view.subviews(self.mapView)
        self.mapView.fillContainer()
    }
    
    private func centerToLocation(_ location: CLLocation) {
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        self.mapView.setRegion(coordinateRegion, animated: true)
    }

    //
    // MARK: - View model bindable
    //

    private func bindViewModel() {
        let outputs = self.viewModel.outputs
        
        outputs.location
            .sink(receiveValue: { [weak self] location in
                self?.centerToLocation(location)
            })
            .store(in: &self.cancellables)
        
        outputs.annotations
            .sink(receiveValue: { [weak self] annotations in
                self?.mapDelegate.addAnnotations(annotations)
            })
            .store(in: &self.cancellables)
    }
    
}

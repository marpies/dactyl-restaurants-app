//
//  MapViewModel.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright (c) 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import Combine
import CoreLocation
import MapKit

protocol MapViewModelInputs {

}

protocol MapViewModelOutputs {
    var location: AnyPublisher<CLLocation, Never> { get }
    var annotations: AnyPublisher<[MKAnnotation], Never> { get }
}

protocol MapViewModelProtocol {
    var inputs: MapViewModelInputs { get }
    var outputs: MapViewModelOutputs { get }
    
    func updateModel(_ model: Content.Model.Response)
}

class MapViewModel: MapViewModelProtocol, MapViewModelInputs, MapViewModelOutputs {

    var inputs: MapViewModelInputs { return self }
    var outputs: MapViewModelOutputs { return self }

    // Inputs

    // Outputs
    private let _location: CurrentValueSubject<CLLocation, Never>
    let location: AnyPublisher<CLLocation, Never>
    
    private let _annotations: CurrentValueSubject<[MKAnnotation], Never> = CurrentValueSubject([])
    let annotations: AnyPublisher<[MKAnnotation], Never>
    
    init(location: CLLocation) {
        _location = CurrentValueSubject(location)
        
        // Outputs
        self.location = _location.eraseToAnyPublisher()
        self.annotations = _annotations
            .filter { $0.isEmpty == false }
            .eraseToAnyPublisher()
    }
    
    func updateModel(_ model: Content.Model.Response) {
        // Create map annotations
        let annotations = model.restaurants.map { (r: Restaurant.Response) -> RestaurantAnnotation in
            let priceRangeFormat = NSLocalizedString("priceRangeText", comment: "")
            let priceRange = String(format: priceRangeFormat, r.priceRange)
            let subtitle = "\(r.address)\n\(priceRange)"
            let coord = CLLocationCoordinate2D(latitude: r.latitude, longitude: r.longitude)
            return RestaurantAnnotation(title: r.name, subtitle: subtitle, coordinate: coord)
        }
        _annotations.send(annotations)
        
        // Set location to the closest restaurant
        let currentLocation = model.location
        if let restaurant = model.restaurants.min(by: { r1, r2 in
            let loc1 = CLLocation(latitude: r1.latitude, longitude: r1.longitude)
            let loc2 = CLLocation(latitude: r2.latitude, longitude: r2.longitude)
            let d1 = loc1.distance(from: currentLocation)
            let d2 = loc2.distance(from: currentLocation)
            return d1 < d2
        }) {
            let location = CLLocation(latitude: restaurant.latitude, longitude: restaurant.longitude)
            _location.send(location)
        }
    }

}

//
//  MapAssembly.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright (c) 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import Swinject
import CoreLocation

struct MapAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(MapViewModelProtocol.self) { (r: Resolver, location: CLLocation) in
            return MapViewModel(location: location)
        }
        container.register(MapViewController.self) { (r: Resolver, vm: MapViewModelProtocol) in
            return MapViewController(viewModel: vm)
        }
    }
    
}

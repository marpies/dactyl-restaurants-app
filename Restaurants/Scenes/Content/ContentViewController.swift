//
//  ContentViewController.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright (c) 2022 Marcel Piešťanský. All rights reserved.
//

import UIKit
import Combine

class ContentViewController: UITabBarController {
    
    private let viewModel: ContentViewModelProtocol
    
    private var cancellables: Set<AnyCancellable> = []
    private var didAppear: Bool = false
    
    private var alert: UIAlertController?

    init(viewModel: ContentViewModelProtocol) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .systemBackground
        self.tabBar.isTranslucent = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard self.didAppear == false else { return }
        
        self.didAppear = true
        
        self.bindViewModel()
        
        let inputs = self.viewModel.inputs
        inputs.viewDidAppear.send()
    }
    
    //
    // MARK: - Private
    //
    
    private func updateAlert(viewModel: Content.Alert.ViewModel?) {
        if let vm = viewModel {
            if self.alert != nil {
                self.dismiss(animated: false, completion: { [weak self] in
                    self?.presentAlert(viewModel: vm)
                })
            } else {
                self.presentAlert(viewModel: vm)
            }
        } else if self.alert != nil {
            self.dismiss(animated: true, completion: nil)
            self.alert = nil
        }
    }
    
    private func presentAlert(viewModel: Content.Alert.ViewModel) {
        self.alert = UIAlertController(title: viewModel.title, message: viewModel.message, preferredStyle: .alert)
        
        if let button = viewModel.button {
            let inputs = self.viewModel.inputs
            
            self.alert?.addAction(UIAlertAction(title: button, style: .default, handler: { _ in
                inputs.retryLoad.send()
            }))
        }
        
        self.present(self.alert!, animated: true, completion: nil)
    }
    
    //
    // MARK: - View model bindable
    //
    
    private func bindViewModel() {
        let outputs = self.viewModel.outputs
        
        outputs.alert
            .sink(receiveValue: { [weak self] vm in
                self?.updateAlert(viewModel: vm)
            })
            .store(in: &self.cancellables)
    }
    
}

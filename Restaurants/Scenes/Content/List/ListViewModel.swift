//
//  ListViewModel.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright (c) 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import Combine
import CoreLocation

protocol ListViewModelInputs {
    var restaurantTap: PassthroughSubject<Int, Never> { get }
}

protocol ListViewModelOutputs {
    var restaurants: AnyPublisher<[Restaurant.ViewModel], Never> { get }
}

protocol ListViewModelDelegate: AnyObject {
    func restaurantDidTap(_ viewModel: Restaurant.ViewModel)
}

protocol ListViewModelProtocol {
    var inputs: ListViewModelInputs { get }
    var outputs: ListViewModelOutputs { get }
    var delegate: ListViewModelDelegate? { get set }
    
    func updateModel(_ model: Content.Model.Response)
}

class ListViewModel: ListViewModelProtocol, ListViewModelInputs, ListViewModelOutputs {

    var inputs: ListViewModelInputs { return self }
    var outputs: ListViewModelOutputs { return self }
    
    weak var delegate: ListViewModelDelegate?
    
    private var cancellables: Set<AnyCancellable> = []

    // Inputs
    let restaurantTap: PassthroughSubject<Int, Never> = PassthroughSubject()

    // Outputs
    private var _restaurants: CurrentValueSubject<[Restaurant.ViewModel], Never> = CurrentValueSubject([])
    let restaurants: AnyPublisher<[Restaurant.ViewModel], Never>
    
    init() {
        // Outputs
        self.restaurants = _restaurants.eraseToAnyPublisher()
        
        // Inputs
        self.restaurantTap
            .combineLatest(_restaurants)
            .map { (index, restaurants) in
                restaurants[index]
            }
            .sink(receiveValue: { [weak self] restaurant in
                self?.delegate?.restaurantDidTap(restaurant)
            })
            .store(in: &self.cancellables)
    }
    
    func updateModel(_ model: Content.Model.Response) {
        let currentLocation = model.location
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        
        let restaurants = model.restaurants
            .sorted(by: { r1, r2 in
                let loc1 = CLLocation(latitude: r1.latitude, longitude: r1.longitude)
                let loc2 = CLLocation(latitude: r2.latitude, longitude: r2.longitude)
                let d1 = loc1.distance(from: currentLocation)
                let d2 = loc2.distance(from: currentLocation)
                return d1 < d2
            })
            .map { (r: Restaurant.Response) -> Restaurant.ViewModel in
                let priceRangeFormat = NSLocalizedString("priceRangeText", comment: "")
                let priceRange = String(format: priceRangeFormat, r.priceRange)
                let restaurantLocation = CLLocation(latitude: r.latitude, longitude: r.longitude)
                let distanceRaw = restaurantLocation.distance(from: currentLocation) / 1000
                let distanceFormatted = formatter.string(from: NSNumber(value: distanceRaw)) ?? ""
                let distanceFormat = NSLocalizedString("distanceText", comment: "")
                let distance = String(format: distanceFormat, distanceFormatted)
                return Restaurant.ViewModel(id: r.id, name: r.name, address: r.address, priceRange: priceRange, distance: distance)
            }
        
        _restaurants.send(restaurants)
    }
}

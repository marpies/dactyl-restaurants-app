//
//  ListAssembly.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright (c) 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import Swinject

struct ListAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(ListViewModelProtocol.self) { r in
            return ListViewModel()
        }
        container.register(ListViewController.self) { (r: Resolver, vm: ListViewModelProtocol) in
            return ListViewController(viewModel: vm)
        }
    }
    
}

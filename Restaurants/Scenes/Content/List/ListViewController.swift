//
//  ListViewController.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright (c) 2022 Marcel Piešťanský. All rights reserved.
//

import UIKit
import Combine
import Stevia

class ListViewController: UIViewController, UITableViewDelegate {
    
    private let viewModel: ListViewModelProtocol
    private let tableView = UITableView(frame: .zero, style: .plain)
    private lazy var dataSource = RestaurantsTableDataSource(tableView: self.tableView)
    
    private var cancellables: Set<AnyCancellable> = []

    init(viewModel: ListViewModelProtocol) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .systemBackground
        
        self.setupViews()
        self.bindViewModel()
    }
    
    //
    // MARK: - Private
    //
    
    private func setupViews() {
        self.tableView.allowsMultipleSelection = false
        self.tableView.delegate = self
        self.view.subviews(self.tableView)
        self.tableView.fillContainer()
    }
    
    //
    // MARK: - Table view delegate
    //
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let inputs = self.viewModel.inputs
        inputs.restaurantTap.send(indexPath.row)
    }

    //
    // MARK: - View model bindable
    //

    private func bindViewModel() {
        let outputs = self.viewModel.outputs
        
        outputs.restaurants
            .sink(receiveValue: { [weak self] restaurants in
                self?.dataSource.update(viewModel: restaurants)
            })
            .store(in: &self.cancellables)
    }
    
}

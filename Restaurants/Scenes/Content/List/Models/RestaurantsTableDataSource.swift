//
//  RestaurantsTableDataSource.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright © 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import UIKit

class RestaurantsTableDataSource {
    
    private let cellId = "restaurant"
    private let dataSource: UITableViewDiffableDataSource<Int, Restaurant.ViewModel>
    
    init(tableView: UITableView) {
        self.dataSource = UITableViewDiffableDataSource(tableView: tableView, cellProvider: { [cellId] tableView, _, vm in
            let cell: UITableViewCell
            
            if let dequeued = tableView.dequeueReusableCell(withIdentifier: cellId) {
                cell = dequeued
            } else {
                cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellId)
            }
            
            cell.textLabel?.text = vm.name
            cell.detailTextLabel?.text = vm.distance
            
            return cell
        })
        
        tableView.dataSource = self.dataSource
    }
    
    func update(viewModel: [Restaurant.ViewModel]) {
        var snapshot = self.dataSource.snapshot()
        
        if snapshot.numberOfSections == 0 {
            snapshot.appendSections([0])
        }
        
        snapshot.appendItems(viewModel, toSection: 0)
        
        self.dataSource.apply(snapshot, animatingDifferences: false, completion: nil)
    }
    
}

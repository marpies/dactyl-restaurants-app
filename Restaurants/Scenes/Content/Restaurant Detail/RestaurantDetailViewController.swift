//
//  RestaurantDetailViewController.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright (c) 2022 Marcel Piešťanský. All rights reserved.
//

import UIKit
import Combine
import Stevia

class RestaurantDetailViewController: UIViewController {
    
    private let viewModel: RestaurantDetailViewModelProtocol
    
    private let contentStack = UIStackView()
    private let nameLabel = UILabel()
    private let addressLabel = UILabel()
    private let priceRangeLabel = UILabel()
    private let distanceLabel = UILabel()
    
    private var cancellables: Set<AnyCancellable> = []
    
    init(viewModel: RestaurantDetailViewModelProtocol) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .systemBackground
        
        self.setupViews()
        self.bindViewModel()
    }
    
    //
    // MARK: - Private
    //
    
    private func setupViews() {
        self.contentStack.axis = .vertical
        self.contentStack.spacing = 16
        self.contentStack.alignment = .leading
        self.view.subviews(self.contentStack)
        self.contentStack.leading(16).trailing(16).top(16)
        
        self.nameLabel.numberOfLines = 0
        self.nameLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        self.nameLabel.textColor = .label
        self.contentStack.addArrangedSubview(self.nameLabel)
        
        self.addressLabel.numberOfLines = 0
        self.addressLabel.font = UIFont.preferredFont(forTextStyle: .body)
        self.addressLabel.textColor = .secondaryLabel
        self.contentStack.addArrangedSubview(self.addressLabel)
        
        self.priceRangeLabel.numberOfLines = 0
        self.priceRangeLabel.font = UIFont.preferredFont(forTextStyle: .subheadline)
        self.priceRangeLabel.textColor = .tertiaryLabel
        self.contentStack.addArrangedSubview(self.priceRangeLabel)
        
        self.distanceLabel.numberOfLines = 0
        self.distanceLabel.font = UIFont.preferredFont(forTextStyle: .caption1)
        self.distanceLabel.textColor = .tertiaryLabel
        self.contentStack.addArrangedSubview(self.distanceLabel)
    }
    
    //
    // MARK: - View model bindable
    //
    
    private func bindViewModel() {
        let outputs = self.viewModel.outputs.restaurant
        
        outputs
            .map(\.name)
            .map { Optional($0) }
            .assign(to: \.text, on: self.nameLabel)
            .store(in: &self.cancellables)
        
        outputs
            .map(\.address)
            .map { Optional($0) }
            .assign(to: \.text, on: self.addressLabel)
            .store(in: &self.cancellables)
        
        outputs
            .map(\.priceRange)
            .map { Optional($0) }
            .assign(to: \.text, on: self.priceRangeLabel)
            .store(in: &self.cancellables)
        
        outputs
            .map(\.distance)
            .map { Optional($0) }
            .assign(to: \.text, on: self.distanceLabel)
            .store(in: &self.cancellables)
    }
    
}

//
//  RestaurantDetailAssembly.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright (c) 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import Swinject

struct RestaurantDetailAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(RestaurantDetailViewModelProtocol.self) { (r: Resolver, vm: Restaurant.ViewModel) in
            return RestaurantDetailViewModel(viewModel: vm)
        }
        container.register(RestaurantDetailViewController.self) { (r: Resolver, vm: RestaurantDetailViewModelProtocol) in
            return RestaurantDetailViewController(viewModel: vm)
        }
    }
    
}

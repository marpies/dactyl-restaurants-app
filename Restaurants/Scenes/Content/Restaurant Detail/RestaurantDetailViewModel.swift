//
//  RestaurantDetailViewModel.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright (c) 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import Combine

protocol RestaurantDetailViewModelInputs {
    
}

protocol RestaurantDetailViewModelOutputs {
    var restaurant: AnyPublisher<Restaurant.ViewModel, Never> { get }
}

protocol RestaurantDetailViewModelProtocol {
    var inputs: RestaurantDetailViewModelInputs { get }
    var outputs: RestaurantDetailViewModelOutputs { get }
}

class RestaurantDetailViewModel: RestaurantDetailViewModelProtocol, RestaurantDetailViewModelInputs, RestaurantDetailViewModelOutputs {
    
    var inputs: RestaurantDetailViewModelInputs { return self }
    var outputs: RestaurantDetailViewModelOutputs { return self }
    
    // Outputs
    private let _restaurant: CurrentValueSubject<Restaurant.ViewModel, Never>
    let restaurant: AnyPublisher<Restaurant.ViewModel, Never>
    
    init(viewModel: Restaurant.ViewModel) {
        _restaurant = CurrentValueSubject(viewModel)
        self.restaurant = _restaurant.eraseToAnyPublisher()
    }
    
}

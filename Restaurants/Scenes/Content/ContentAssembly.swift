//
//  ContentAssembly.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright (c) 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import Swinject
import CoreLocation

struct ContentAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(ContentViewModelProtocol.self) { (r: Resolver, location: CLLocation) in
            let apiService = r.resolve(APIService.self)!
            return ContentViewModel(location: location, apiService: apiService)
        }
        container.register(ContentViewController.self) { (r: Resolver, vm: ContentViewModelProtocol) in
            return ContentViewController(viewModel: vm)
        }
    }
    
}

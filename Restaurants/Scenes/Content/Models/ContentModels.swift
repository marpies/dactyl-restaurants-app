//
//  ContentModels.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright © 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import CoreLocation

enum Content {
    
    enum State {
        case loading, error, success
    }
    
    enum Alert {
        
        struct ViewModel {
            let title: String
            let message: String?
            let button: String?
        }
        
    }
    
    enum Model {
        
        struct Response {
            let location: CLLocation
            let restaurants: [Restaurant.Response]
        }
        
    }
    
}

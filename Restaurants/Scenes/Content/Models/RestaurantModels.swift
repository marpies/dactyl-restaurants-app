//
//  RestaurantModels.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright © 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation

enum Restaurants {
    
    struct Response: Decodable {
        let restaurants: [Restaurant.Response]
        
        enum RootKeys: String, CodingKey {
            case restaurants
        }
    }
    
}

enum Restaurant {
    
    struct Response: Decodable {
        let id: String
        let name: String
        let address: String
        let latitude: Double
        let longitude: Double
        let priceRange: Int
        
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: RootKeys.self)
            let restaurant = try container.nestedContainer(keyedBy: RestaurantKeys.self, forKey: .restaurant)
            
            self.id = try restaurant.decode(String.self, forKey: .id)
            self.name = try restaurant.decode(String.self, forKey: .name)
            self.priceRange = try restaurant.decode(Int.self, forKey: .priceRange)
            
            let location = try restaurant.nestedContainer(keyedBy: LocationKeys.self, forKey: .location)
            
            self.address = try location.decode(String.self, forKey: .address)
            
            let latitudeRaw = try location.decode(String.self, forKey: .latitude)
            let longitudeRaw = try location.decode(String.self, forKey: .longitude)
            
            self.latitude = Double(latitudeRaw) ?? 0
            self.longitude = Double(longitudeRaw) ?? 0
        }
        
        enum RootKeys: String, CodingKey {
            case restaurant
        }
        
        enum RestaurantKeys: String, CodingKey {
            case id, name, location, priceRange = "price_range"
        }
        
        enum LocationKeys: String, CodingKey {
            case address, latitude, longitude
        }
    }
    
    class ViewModel: Hashable {
        let id: String
        let name: String
        let address: String
        let priceRange: String
        let distance: String

        init(id: String, name: String, address: String, priceRange: String, distance: String) {
            self.id = id
            self.name = name
            self.address = address
            self.priceRange = priceRange
            self.distance = distance
        }
        
        func hash(into hasher: inout Hasher) {
            hasher.combine(self.id)
        }
        
        static func == (lhs: Restaurant.ViewModel, rhs: Restaurant.ViewModel) -> Bool {
            return lhs.id == rhs.id
        }
    }
    
}

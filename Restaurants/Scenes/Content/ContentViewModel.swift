//
//  ContentViewModel.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright (c) 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import Combine
import CoreLocation

protocol ContentViewModelInputs {
    var viewDidAppear: PassthroughSubject<Void, Never> { get }
    var retryLoad: PassthroughSubject<Void, Never> { get }
}

protocol ContentViewModelOutputs {
    var state: AnyPublisher<Content.State, Never> { get }
    var alert: AnyPublisher<Content.Alert.ViewModel?, Never> { get }
}

protocol ContentViewModelDelegate: AnyObject {
    func contentModelDidLoad(_ model: Content.Model.Response)
}

protocol ContentViewModelProtocol {
    var inputs: ContentViewModelInputs { get }
    var outputs: ContentViewModelOutputs { get }
    var delegate: ContentViewModelDelegate? { get set }
}

class ContentViewModel: ContentViewModelProtocol, ContentViewModelInputs, ContentViewModelOutputs {

    var inputs: ContentViewModelInputs { return self }
    var outputs: ContentViewModelOutputs { return self }
    
    weak var delegate: ContentViewModelDelegate?
    
    private var cancellables: Set<AnyCancellable> = []
    private let apiService: APIService

    // Inputs
    let viewDidAppear: PassthroughSubject<Void, Never> = PassthroughSubject()
    let retryLoad: PassthroughSubject<Void, Never> = PassthroughSubject()

    // Outputs
    private let _state: CurrentValueSubject<Content.State, Never> = CurrentValueSubject(.loading)
    let state: AnyPublisher<Content.State, Never>
    
    private let _alert: CurrentValueSubject<Content.Alert.ViewModel?, Never> = CurrentValueSubject(nil)
    let alert: AnyPublisher<Content.Alert.ViewModel?, Never>
    
    init(location: CLLocation, apiService: APIService) {
        self.apiService = apiService
        
        // Outputs
        self.state = _state.eraseToAnyPublisher()
        self.alert = _alert.eraseToAnyPublisher()
        
        self.retryLoad
            .sink(receiveValue: { [weak self] in
                self?._state.send(.loading)
            })
            .store(in: &self.cancellables)
        
        let apiRequest = Publishers.Merge(self.viewDidAppear.prefix(1), self.retryLoad)
            .flatMap {
                apiService.loadRestaurants()
                    .map { Optional($0) }
                    .replaceError(with: nil)
            }
            .receive(on: DispatchQueue.main)
            .share()
        
        apiRequest
            .filter { $0 == nil }
            .sink(receiveValue: { [weak self] _ in
                self?._state.send(.error)
            })
            .store(in: &self.cancellables)
        
        apiRequest
            .filter { $0 != nil }
            .sink(receiveValue: { [weak self] _ in
                self?._state.send(.success)
            })
            .store(in: &self.cancellables)
        
        apiRequest
            .compactMap { $0 }
            .sink(receiveValue: { [weak self] restaurants in
                let model = Content.Model.Response(location: location, restaurants: restaurants)
                self?.delegate?.contentModelDidLoad(model)
            })
            .store(in: &self.cancellables)
        
        _state
            .map { [weak self] state in
                self?.getAlert(forState: state)
            }
            .assign(to: \.value, on: _alert)
            .store(in: &self.cancellables)
    }
    
    //
    // MARK: - Private
    //
    
    private func getAlert(forState state: Content.State) -> Content.Alert.ViewModel? {
        switch state {
        case .loading:
            let title = NSLocalizedString("contentLoadingTitle", comment: "")
            return Content.Alert.ViewModel(title: title, message: nil, button: nil)
        case .error:
            let title = NSLocalizedString("contentLoadErrorTitle", comment: "")
            let message = NSLocalizedString("contentLoadMessageTitle", comment: "")
            let button = NSLocalizedString("contentLoadRetryButton", comment: "")
            return Content.Alert.ViewModel(title: title, message: message, button: button)
        case .success:
            return nil
        }
    }

}

//
//  SetupViewController.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright (c) 2022 Marcel Piešťanský. All rights reserved.
//

import UIKit
import Combine
import Stevia
import CombineCocoa

class SetupViewController: UIViewController {
    
    private let viewModel: SetupViewModelProtocol
    
    private let contentStack = UIStackView()
    private let messageLabel = UILabel()
    private var spinnerView = UIActivityIndicatorView(style: .medium)
    private var openSettingsButton: UIButton?
    
    private var cancellables: Set<AnyCancellable> = []

    init(viewModel: SetupViewModelProtocol) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .systemBackground
        
        self.setupViews()
        self.bindViewModel()
    }
    
    //
    // MARK: - Private
    //
    
    private func setupViews() {
        self.contentStack.style { stack in
            stack.axis = .vertical
            stack.spacing = 8
            stack.alignment = .center
        }
        self.view.subviews(self.contentStack)
        self.contentStack.Leading == self.view.layoutMarginsGuide.Leading
        self.contentStack.Trailing == self.view.layoutMarginsGuide.Trailing
        self.contentStack.CenterY == self.view.layoutMarginsGuide.CenterY
        
        self.messageLabel.style { label in
            label.font = .preferredFont(forTextStyle: .body)
            label.textColor = .label
            label.numberOfLines = 0
        }
        self.contentStack.addArrangedSubview(self.messageLabel)
    }
    
    private func setSpinnerVisibility(_ visible: Bool) {
        if visible {
            self.spinnerView.startAnimating()
            self.contentStack.addArrangedSubview(self.spinnerView)
        } else {
            self.spinnerView.stopAnimating()
            self.spinnerView.removeFromSuperview()
        }
    }
    
    private func setButtonVisibility(_ visible: Bool) {
        if visible {
            if self.openSettingsButton == nil {
                self.addButton()
            }
        } else {
            self.openSettingsButton?.removeFromSuperview()
            self.openSettingsButton = nil
        }
    }
    
    private func addButton() {
        self.openSettingsButton = UIButton(type: .system)
        self.contentStack.addArrangedSubview(self.openSettingsButton!)
        
        let outputs = self.viewModel.outputs
        
        outputs.buttonTitle
            .sink(receiveValue: { [openSettingsButton] title in
                openSettingsButton?.setTitle(title, for: .normal)
            })
            .store(in: &self.cancellables)
        
        let inputs = self.viewModel.inputs
        
        self.openSettingsButton?.tapPublisher
            .sink(receiveValue: {
                inputs.openSettings.send(())
            })
            .store(in: &self.cancellables)
    }

    //
    // MARK: - View model bindable
    //

    private func bindViewModel() {
        let outputs = self.viewModel.outputs
        
        outputs.message
            .map { Optional($0) }
            .assign(to: \.text, on: self.messageLabel)
            .store(in: &self.cancellables)
        
        outputs.state
            .map { $0 == .pending }
            .sink(receiveValue: { [weak self] isLoading in
                self?.setSpinnerVisibility(isLoading)
            })
            .store(in: &self.cancellables)
        
        outputs.state
            .map { $0 == .denied }
            .sink(receiveValue: { [weak self] isDenied in
                self?.setButtonVisibility(isDenied)
            })
            .store(in: &self.cancellables)
    }
    
}

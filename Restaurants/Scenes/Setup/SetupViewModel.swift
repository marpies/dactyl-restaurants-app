//
//  SetupViewModel.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright (c) 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import Combine
import CoreLocation
import UIKit

protocol SetupViewModelInputs {
    var openSettings: PassthroughSubject<Void, Never> { get }
}

protocol SetupViewModelOutputs {
    var message: AnyPublisher<String, Never> { get }
    var state: AnyPublisher<Setup.State, Never> { get }
    var buttonTitle: AnyPublisher<String, Never> { get }
}

protocol SetupViewModelDelegate: AnyObject {
    func setupDidInitialize(with location: CLLocation)
}

protocol SetupViewModelProtocol {
    var inputs: SetupViewModelInputs { get }
    var outputs: SetupViewModelOutputs { get }
    var delegate: SetupViewModelDelegate? { get set }
}

class SetupViewModel: SetupViewModelProtocol, SetupViewModelInputs, SetupViewModelOutputs {

    var inputs: SetupViewModelInputs { return self }
    var outputs: SetupViewModelOutputs { return self }
    var delegate: SetupViewModelDelegate?
    
    private let locationManager: LocationManager
    
    private var cancellables: Set<AnyCancellable> = []

    // Inputs
    let openSettings: PassthroughSubject<Void, Never> = PassthroughSubject()

    // Outputs
    private let _message: CurrentValueSubject<String, Never> = CurrentValueSubject(NSLocalizedString("requestingLocationMessage", comment: ""))
    let message: AnyPublisher<String, Never>
    
    private let _state: CurrentValueSubject<Setup.State, Never> = CurrentValueSubject(.pending)
    let state: AnyPublisher<Setup.State, Never>
    
    private let _buttonTitle: CurrentValueSubject<String, Never> = CurrentValueSubject(NSLocalizedString("openSettingsButton", comment: ""))
    let buttonTitle: AnyPublisher<String, Never>
    
    init(locationManager: LocationManager) {
        self.locationManager = locationManager
        
        // Outputs
        self.message = _message.eraseToAnyPublisher()
        self.state = _state.eraseToAnyPublisher()
        self.buttonTitle = _buttonTitle.eraseToAnyPublisher()
        
        // Inputs
        self.openSettings
            .sink(receiveValue: { [weak self] in
                self?.openAppSettings()
            })
            .store(in: &self.cancellables)
        
        _state
            .sink(receiveValue: { [weak self] state in
                self?.updateMessage(state)
            })
            .store(in: &self.cancellables)
        
        let authorizationStatus = locationManager.authorizationStatus.share()
        
        authorizationStatus
            .map { (status: CLAuthorizationStatus) -> Setup.State in
                switch status {
                case .notDetermined, .authorizedAlways, .authorizedWhenInUse:
                    return .pending
                case .restricted, .denied:
                    return .denied
                @unknown default:
                    return .denied
                }
            }
            .sink(receiveValue: { [weak self] state in
                self?._state.send(state)
            })
            .store(in: &self.cancellables)
        
        authorizationStatus
            .filter { $0 == .notDetermined }
            .sink(receiveValue: { _ in
                locationManager.requestLocation()
            })
            .store(in: &self.cancellables)
        
        let currentLocation = locationManager.currentLocation.share()
        
        currentLocation
            .map { (_) -> Setup.State in
                return .complete
            }
            .sink(receiveValue: { [weak self] state in
                self?._state.send(state)
            })
            .store(in: &self.cancellables)
        
        currentLocation
            .sink(receiveValue: { [weak self] (location: CLLocation) in
                self?.delegate?.setupDidInitialize(with: location)
            })
            .store(in: &self.cancellables)
    }
    
    //
    // MARK: - Private
    //
    
    private func updateMessage(_ state: Setup.State) {
        switch state {
        case .pending:
            _message.send(NSLocalizedString("requestingLocationMessage", comment: ""))
        case .denied:
            _message.send(NSLocalizedString("locationDidDenyMessage", comment: ""))
        case .complete:
            _message.send(NSLocalizedString("locationDidLoadMessage", comment: ""))
        }
    }
    
    private func openAppSettings() {
        if let appSettings = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(appSettings, options: [:], completionHandler: nil)
        }
    }

}

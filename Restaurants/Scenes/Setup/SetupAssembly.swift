//
//  SetupAssembly.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright (c) 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import Swinject

struct SetupAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(SetupViewModelProtocol.self) { r in
            let locationManager = r.resolve(LocationManager.self)!
            return SetupViewModel(locationManager: locationManager)
        }
        container.register(SetupViewController.self) { (r: Resolver, vm: SetupViewModelProtocol) in
            return SetupViewController(viewModel: vm)
        }
    }
    
}

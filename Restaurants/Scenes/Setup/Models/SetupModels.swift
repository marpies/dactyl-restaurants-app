//
//  SetupModels.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright © 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation

enum Setup {
    
    enum State {
        case pending, denied, complete
    }
    
}

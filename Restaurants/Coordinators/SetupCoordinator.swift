//
//  SetupCoordinator.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright © 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import UIKit
import Swinject
import CoreLocation

class SetupCoordinator: Coordinator, SetupViewModelDelegate {
    
    private let resolver: Resolver
    private let navigationController: UINavigationController
    
    var children: [Coordinator] = []
    
    init(assembler: Assembler) {
        self.resolver = assembler.resolver
        self.navigationController = UINavigationController()
        
        self.setupAssembler(assembler)
    }
    
    @discardableResult func start() -> UIViewController {
        var setupVM = self.resolver.resolve(SetupViewModelProtocol.self)!
        setupVM.delegate = self
        let setupVC = self.resolver.resolve(SetupViewController.self, argument: setupVM)!
        
        self.navigationController.setNavigationBarHidden(true, animated: false)
        self.navigationController.setViewControllers([setupVC], animated: false)
        
        return self.navigationController
    }
    
    //
    // MARK: - Setup delegate
    //
    
    func setupDidInitialize(with location: CLLocation) {
        let coordinator = ContentCoordinator(location: location, resolver: self.resolver)
        self.children.append(coordinator)

        let vc = coordinator.start()

        self.navigationController.setViewControllers([vc], animated: true)
    }
    
    //
    // MARK: - Private
    //
    
    private func setupAssembler(_ assembler: Assembler) {
        assembler.apply(assemblies: [
            SetupAssembly(),
            ContentAssembly(),
            MapAssembly(),
            ListAssembly(),
            RestaurantDetailAssembly(),
            ServicesAssembly()
        ])
    }
    
}

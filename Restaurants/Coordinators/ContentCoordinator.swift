//
//  ContentCoordinator.swift
//  Restaurants
//
//  Created by Marcel Piešťanský on 21.05.2022.
//  Copyright © 2022 Marcel Piešťanský. All rights reserved.
//

import Foundation
import Swinject
import CoreLocation
import UIKit

class ContentCoordinator: Coordinator, ContentViewModelDelegate, ListViewModelDelegate {
    
    let location: CLLocation
    let resolver: Resolver
    
    var children: [Coordinator] = []
    
    private var contentVC: ContentViewController?
    private var mapVM: MapViewModelProtocol?
    private var listVM: ListViewModelProtocol?
    
    init(location: CLLocation, resolver: Resolver) {
        self.location = location
        self.resolver = resolver
    }
    
    @discardableResult func start() -> UIViewController {
        var contentVM = self.resolver.resolve(ContentViewModelProtocol.self, argument: self.location)!
        contentVM.delegate = self
        let contentVC = self.resolver.resolve(ContentViewController.self, argument: contentVM)!
        
        let mapVM = self.resolver.resolve(MapViewModelProtocol.self, argument: self.location)!
        let mapVC = self.resolver.resolve(MapViewController.self, argument: mapVM)!
        
        var listVM = self.resolver.resolve(ListViewModelProtocol.self)!
        listVM.delegate = self
        let listVC = self.resolver.resolve(ListViewController.self, argument: listVM)!
        
        mapVC.tabBarItem = UITabBarItem(title: NSLocalizedString("mapTabTitle", comment: ""), image: UIImage(systemName: "map"), tag: 0)
        listVC.tabBarItem = UITabBarItem(title: NSLocalizedString("listTabTitle", comment: ""), image: UIImage(systemName: "list.bullet"), tag: 1)
        
        contentVC.viewControllers = [mapVC, listVC]
        
        self.mapVM = mapVM
        self.listVM = listVM
        self.contentVC = contentVC
        
        return contentVC
    }
    
    //
    // MARK: - Content delegate
    //
    
    func contentModelDidLoad(_ model: Content.Model.Response) {
        self.mapVM?.updateModel(model)
        self.listVM?.updateModel(model)
    }
    
    //
    // MARK: - List delegate
    //
    
    func restaurantDidTap(_ viewModel: Restaurant.ViewModel) {
        let detailVM = self.resolver.resolve(RestaurantDetailViewModelProtocol.self, argument: viewModel)!
        let detailVC = self.resolver.resolve(RestaurantDetailViewController.self, argument: detailVM)!
        detailVC.modalPresentationStyle = .pageSheet
        self.contentVC?.present(detailVC, animated: true, completion: nil)
    }
    
}
